export const initState = {
  rooms: [],
  lastMessage: null,
  user: null,
  userLoggedIn: false,
};

export const reducer = (state = initState, action) => {
  let newState = state;
  switch (action.type) {
    case "SET_USER":
      newState = {
        ...newState,
        user: action.user,
        userLoggedIn: action.userLoggedIn,
      };
      break;

    case "INIT-ROOMS":
      newState = {
        ...newState,
        rooms: action.rooms,
      };
      break;
    case "SET-NEW-ROOM":
      newState = {
        ...newState,
        rooms: [...newState.rooms, action.room],
      };
      break;
    case "INIT-ROOM-MESSAGES":
      let updatedRooms = newState.rooms.map((room) => {
        if (room.id === action.roomId) {
          room.messages = action.messages;
          return room;
        }

        return room;
      });
      newState = {
        ...newState,
        rooms: updatedRooms,
      };
      break;

    case "ADD-NEW-MESSAGE-TO-ROOM":
      const rooms = newState.rooms.map((room) => {
        if (room.id === action.roomId) {
          room.messages.push(action.message);
          return room;
        }

        return room;
      });

      newState = {
        ...newState,
        rooms: rooms,
      };
      break;

    case "DELETE-SELECTED-ROOM":
      newState = {
        ...newState,
        rooms: newState.rooms.filter((room) => room.id !== action.roomId),
      };
      break;

    default:
      break;
  }
  return newState;
};
