import { useReducer, useContext, useEffect } from "react";

import AppContext from "./appContext";
import { initState, reducer } from "./appState";
import HttpService from "../services/http";
import PusherService from "../services/pusher";

const pusherService = new PusherService();

export const AppProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initState);

  useEffect(() => {
    pusherService.subscribe("app");
  }, []);

  return (
    <AppContext.Provider
      value={{
        state,
        dispatch,
        httpService: new HttpService(),
        pusherService: pusherService,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export const useAppState = () => {
  return useContext(AppContext);
};
