import firebase from "firebase/app";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyB5VXlw5NY02JGHoj6VDNeLPZzhe2_chtQ",
  authDomain: "whatsapp-clone-e5e4b.firebaseapp.com",
  projectId: "whatsapp-clone-e5e4b",
  storageBucket: "whatsapp-clone-e5e4b.appspot.com",
  messagingSenderId: "163924966547",
  appId: "1:163924966547:web:10af2e7886c27ae9d86166",
  measurementId: "G-KS21F66G51",
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();

export const googleProvider = new firebase.auth.GoogleAuthProvider()

