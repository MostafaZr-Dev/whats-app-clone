class AudioManager {
  constructor() {
    this.chunk = [];
    this.blob = null;
    this.mediaRecorder = null;
    this.audioUrl = null;
  }

  async getUserMediaPermission() {
    try {
      const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
      return stream;
    } catch (error) {
      return error;
    }
  }

  setMediaRecorder(stream) {
    this.mediaRecorder = new MediaRecorder(stream);

    this.mediaRecorder.addEventListener("dataavailable", (e) => {
      this.chunk = [];
      this.chunk.push(e.data);
    });
    this.mediaRecorder.addEventListener("stop", (e) => {
      console.log("stop");

      this.blob = new Blob(this.chunk, { type: "audio/wav" });
      this.audioUrl = URL.createObjectURL(this.blob);
    });
  }

  startMediaRecorder() {
    this.mediaRecorder.start();
  }

  stopMediaRecorder() {
    this.mediaRecorder.stop();
  }

  getBlob() {
    return this.blob;
  }

  getAudioUrl() {
    return this.audioUrl;
  }
}

export default AudioManager;
