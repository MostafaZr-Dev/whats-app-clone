import Pusher from "pusher-js";

class PusherService {
  constructor() {
    this.channel = null;
    this.pusher = new Pusher('223a39cc2569905bba44', {
      cluster: 'eu'
    });
  }

  subscribe(channel) {
    this.channel = this.pusher.subscribe(channel);
  }

  bind(name, cb) {
    this.channel.bind(name, cb);
  }
}

export default PusherService;
