import React from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";

export default function Modal({
  isOpen,
  onClose,
  title,
  content,
  onCancelAction,
  onAcceptAction,
  contentText,
  btnAcceptColor,
  btnAcceptText,
}) {
  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      fullWidth={true}
      maxWidth="md"
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{contentText}</DialogContentText>
        {content}
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancelAction} color="primary">
          Cancel
        </Button>
        <Button onClick={onAcceptAction} color={btnAcceptColor}>
          {btnAcceptText}
        </Button>
      </DialogActions>
    </Dialog>
  );
}
