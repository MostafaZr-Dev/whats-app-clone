import { useEffect, useState, useCallback } from "react";
import { useParams, useHistory } from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";
import "./Chat.css";

import ChatHeader from "./Components/ChatHeader";
import ChatBody from "./Components/ChatBody";
import ChatFooter from "./Components/ChatFooter";
import { useAppState } from "../../store/useAppState";

function Chat() {
  const [dummyMessage, setDummyMessage] = useState(null);
  const [roomName, setRoomName] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const [audioUrl, setAudioUrl] = useState(null);
  const [isUpload, setIsUpload] = useState(false);
  const [uploadProgress, setUploadProgress] = useState(0);
  const [selectedImage, setSelectedImage] = useState({
    file: null,
    url: null,
  });

  const {
    httpService,
    pusherService,
    dispatch,
    state: { user, rooms },
  } = useAppState();

  const { roomId } = useParams();
  let history = useHistory();

  useEffect(() => {
    setIsLoading(true);

    httpService
      .get(`/room/${roomId}`)
      .then((response) => {
        if (response.data) {
          setRoomName(response.data.name);
          dispatch({
            type: "INIT-ROOM-MESSAGES",
            roomId,
            messages: response.data.messages,
          });
        }
        setIsLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setIsLoading(false);
        history.push("/");
      });
  }, [roomId]);

  useEffect(() => {
    pusherService.bind("new-room-message", (data) => {
      setDummyMessage(null);
      setIsUpload(false);
      
      dispatch({
        type: "ADD-NEW-MESSAGE-TO-ROOM",
        roomId: data.roomId,
        message: data.message,
      });
    });
  }, []);

  const onSendMessage = useCallback(
    async (message) => {
      setDummyMessage(message);

      await httpService.post(`/room/${roomId}/new`, {
        id: user.id,
        name: user.userName,
        message: message,
        timestamp: new Date(),
      });

      dispatch({
        type: "SET-LAST-ROOM-MESSAGE",
        message: message,
      });
    },
    [roomId]
  );

  const onSendAudioMessage = (audioUrl, audioBlob) => {
    setAudioUrl(audioUrl);

    const config = {
      onUploadProgress: function (progressEvent) {
        const percentCompleted = Math.round(
          (progressEvent.loaded * 100) / progressEvent.total
        );

        setUploadProgress(percentCompleted);
      },
    };

    const formData = new FormData();
    formData.append("id", user.id);
    formData.append("name", user.userName);
    formData.append("timestamp", new Date());
    formData.append("file", audioBlob, "audio.wav");

    setIsUpload(true);
    httpService
      .post(`/room/${roomId}/upload`, formData, config)
      .then((res) => {
        setIsUpload(false);
        setAudioUrl(null);
        console.log(res);
      })
      .catch((err) => console.log(err));
  };

  const onSendImage = () => {
    const image = selectedImage.file;
    setSelectedImage((prevState) => ({ ...prevState, file: null }));

    const config = {
      onUploadProgress: function (progressEvent) {
        const percentCompleted = Math.round(
          (progressEvent.loaded * 100) / progressEvent.total
        );

        setUploadProgress(percentCompleted);
      },
    };

    const formData = new FormData();
    formData.append("id", user.id);
    formData.append("name", user.userName);
    formData.append("timestamp", new Date());
    formData.append("file", image);

    setIsUpload(true);
    httpService
      .post(`/room/${roomId}/upload`, formData, config)
      .then((res) => {
        setIsUpload(false);
        setSelectedImage({ file: null, url: null });
      })
      .catch((err) => console.log(err));
  };

  const onCancelSendImage = useCallback((e) => {
    setSelectedImage({
      file: null,
      url: null,
    });
  }, []);

  const onSelectImage = useCallback((selectedImage, url) => {
    setSelectedImage({
      file: selectedImage,
      url: url,
    });
  }, []);

  if (isLoading) {
    return (
      <div className="chat">
        <CircularProgress
          style={{ margin: "15px auto", color: "rgb(50 156 63)" }}
        />
      </div>
    );
  }

  const currentRoom = rooms.filter((room) => room.id === roomId).shift();

  return (
    <div className="chat">
      <ChatHeader
        roomName={roomName}
        lastSeen={
          currentRoom?.messages.length >= 1
            ? currentRoom?.messages[currentRoom?.messages.length - 1].timestamp
            : null
        }
        onSelectImage={onSelectImage}
      />
      <ChatBody
        roomId={roomId}
        messages={currentRoom?.messages}
        dummyMessage={dummyMessage}
        audioUrl={audioUrl}
        isUpload={isUpload}
        uploadProgress={uploadProgress}
        selectedImage={selectedImage}
        onCancelSendImage={onCancelSendImage}
        onSendImage={onSendImage}
        roomName={currentRoom?.name}
      />
      <ChatFooter
        onSendMessage={onSendMessage}
        onSendAudioMessage={onSendAudioMessage}
      />
    </div>
  );
}

export default Chat;
