
function AttachItem({icon}) {
    return (
        <div className="attach__item">
            {icon}
        </div>
    )
}

export default AttachItem
