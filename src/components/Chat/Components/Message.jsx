import { CircularProgress ,Checkbox} from "@material-ui/core";

function Message({ name, message, timestamp, isReciver, isLoading }) {
  return (
    <>
      <p className={`chat__message ${isReciver ? "chat__reciver" : ""}`}>
        <span className="chat__name">{name}</span>

        {isLoading && (
          <CircularProgress
            size={10}
            style={{
              margin: "15px auto",
              color: "#333",
              position: "absolute",
              left: "-13px",
              top: "0",
            }}
          />
        )}
        {message}
        <span className="chat__timestamp">{timestamp}</span>
      </p>
    </>
  );
}

export default Message;
