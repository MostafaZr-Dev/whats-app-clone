import { Close, Image } from "@material-ui/icons";
import Progress from "./Progress";
import Skeleton from "@material-ui/lab/Skeleton";
import { useState } from "react";

function ImageMessage({
  name,
  isReciver,
  src,
  isUpload,
  uploadProgress,
  timestamp,
  onClick
}) {
  const [isImageLoad, setIsImageLoad] = useState(false);

  const onLoadImage = () => {
    setIsImageLoad(true);
  };

  return (
    <p
      className={`chat__message ${
        isReciver ? "chat__reciver" : ""
      } chat__image`}
      onClick={onClick}
    >
      <span className="chat__name">{name}</span>
      {!isImageLoad && (
        <Skeleton variant="rect" width={210} height={118}>
          <Image />
        </Skeleton>
      )}
      <img src={src} alt="imageMessage" onLoad={onLoadImage} />
      <div className="image__info">
        {isUpload && (
          <Progress value={uploadProgress} size={20}>
            <Close style={{ fontSize: "x-small" }} />
          </Progress>
        )}
        <span>{timestamp}</span>
      </div>
    </p>
  );
}

export default ImageMessage;
