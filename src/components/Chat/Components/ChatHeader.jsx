import { useEffect, useState, useCallback, useRef } from "react";
import { Avatar, IconButton, ClickAwayListener, Fade } from "@material-ui/core";
import {
  AttachFile,
  MoreVert,
  SearchOutlined,
  Image,
} from "@material-ui/icons";

import { formatDate } from "../../../services/date";
import AttachItem from "./AttachItem";

function ChatHeader({ roomName, lastSeen, onSelectImage }) {
  const [avatar, setAvatar] = useState("");
  const [isAttachVisible, setIsAttachVisible] = useState(false);

  const fileRef = useRef();

  useEffect(() => {
    setAvatar(Math.floor(Math.random() * 5000));
  }, []);

  const handleAttachClick = useCallback(() => {
    setIsAttachVisible((prevState) => !prevState);
  }, []);

  const handleSendImage = useCallback((e) => {
    fileRef.current.click();
  }, []);

  const handleOutsideAttach = useCallback((e) => {
    setIsAttachVisible(false);
  }, []);

  const handleChangeFile = (e) => {
    const isFileSelect = e.target.files.length >= 1;
    console.log(isFileSelect);
    if (isFileSelect) {
      const selectedFile = e.target.files[0];
      const url = URL.createObjectURL(selectedFile);
      setIsAttachVisible(false);
      onSelectImage(selectedFile, url);
    }
  };

  return (
    <div className="chat__header">
      <Avatar
        src={`https://avatars.dicebear.com/api/avataaars/${avatar}.svg`}
      />
      <div className="chat__headerInfo">
        <h3>{roomName}</h3>
        <p>{lastSeen ? formatDate(lastSeen) : "lastSeen"}</p>
      </div>
      <div className="chat__headerRight">
        <IconButton>
          <SearchOutlined />
        </IconButton>
        <div className="chat__attach">
          <IconButton onClick={handleAttachClick}>
            <AttachFile />
          </IconButton>

          {isAttachVisible && (
            <ClickAwayListener onClickAway={handleOutsideAttach}>
              <div className="attach__wrapper">
                <AttachItem
                  icon={
                    <IconButton onClick={handleSendImage}>
                      <Image />
                    </IconButton>
                  }
                />
              </div>
            </ClickAwayListener>
          )}
        </div>

        <IconButton>
          <MoreVert />
        </IconButton>

        <input
          type="file"
          className="file"
          ref={fileRef}
          onChange={handleChangeFile}
          multiple={false}
        />
      </div>
    </div>
  );
}

export default ChatHeader;
