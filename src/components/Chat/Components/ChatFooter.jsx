import { useState, useCallback, useEffect } from "react";
import {
  InsertEmoticon,
  Mic,
  FiberManualRecord,
  Done,
  Close,
} from "@material-ui/icons";
import { IconButton } from "@material-ui/core";

import AudioManager from "../../../services/audio";
import Notification from "../../Notification";
import EmojiPicker from "./EmojiPicker";

const audioManager = new AudioManager();

function ChatFooter({ onSendMessage, onSendAudioMessage }) {
  const [message, setMessage] = useState("");
  const [isRecord, setIsRecord] = useState(false);
  const [error, setError] = useState(false);
  const [audioUrl, setAudioUrl] = useState(null);
  const [audioBlob, setAudioBlob] = useState(null);
  const [isEmojiPickerVisible, setIsEmojiPickerVisible] = useState(false);

  useEffect(() => {
    if (audioUrl && audioBlob) {
      onSendAudioMessage(audioUrl, audioBlob);
      setAudioUrl(null);
      setAudioBlob(null);
    }
  }, [audioUrl, audioBlob]);

  const sendMessage = (e) => {
    e.preventDefault();
    if (message === "") {
      return;
    }

    onSendMessage(message);

    setMessage("");
  };

  const handleChange = (e) => {
    const value = e.target.value;
    if (value === "" || value.trim() === "") {
      setMessage(value.trim());
      return;
    }
    setMessage(value);
  };

  const handleRecordAudio = (e) => {
    if (!isRecord) {
      audioManager
        .getUserMediaPermission()
        .then((stream) => {
          if (stream.message) {
            setError(stream.message);
            return;
          }
          setIsRecord(true);
          audioManager.setMediaRecorder(stream);
          audioManager.startMediaRecorder();
        })
        .catch((err) => {
          setError(err);
        });
    } else {
      setIsRecord(false);
      audioManager.stopMediaRecorder();
      setTimeout(() => {
        const audioUrl = audioManager.getAudioUrl();
        const blob = audioManager.getBlob();
        setAudioBlob(blob);
        setAudioUrl(audioUrl);
      }, 100);
    }
  };

  const handleCancelRecord = () => {
    setIsRecord(false);
    audioManager.stopMediaRecorder();
  };

  const handleClose = useCallback(() => {
    setError(false);
  }, []);

  const handleClickEmoji = useCallback(() => {
    setIsEmojiPickerVisible((prevState) => !prevState);
  }, []);

  const onEmojiOutsideClick = useCallback(() => {
    setIsEmojiPickerVisible(false);
  }, []);

  const onSelectEmoji = useCallback((emoji) => {
    setMessage((prevState) => `${prevState}${emoji.native}`);
  }, []);

  return (
    <div className="chat__footer">
      <Notification
        onClose={handleClose}
        isOpen={error ? true : false}
        message={error}
        type="error"
      />

      <IconButton onClick={handleClickEmoji}>
        <InsertEmoticon />
      </IconButton>

      <EmojiPicker
        isOpen={isEmojiPickerVisible}
        onOutsideClick={onEmojiOutsideClick}
        onSelect={onSelectEmoji}
      />

      <form>
        <input
          type="text"
          placeholder="Type a message..."
          onChange={handleChange}
          value={message}
        />
        <button onClick={sendMessage}>Send a message</button>
      </form>

      {!isRecord && (
        <IconButton onClick={handleRecordAudio}>
          <Mic />
        </IconButton>
      )}
      {isRecord && (
        <div className="audioRecord">
          <IconButton onClick={handleCancelRecord}>
            <Close />
          </IconButton>
          <FiberManualRecord color="secondary" className="stopRecord" />
          <IconButton onClick={handleRecordAudio}>
            <Done />
          </IconButton>
        </div>
      )}
    </div>
  );
}

export default ChatFooter;
