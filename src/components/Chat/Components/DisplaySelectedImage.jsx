import { IconButton } from "@material-ui/core";
import { Check, Close } from "@material-ui/icons";

function DisplaySelectedImage({src, onClose,onAccept}) {
  return (
    <div className="displaySelectedImage">
      <div className="image">
        <img src={src} alt="showImage" />
      </div>
      <div className="action__btn">
        <IconButton className="close__btn" onClick={onClose}>
          <Close />
        </IconButton>
        <IconButton className="check__btn" onClick={onAccept}>
          <Check />
        </IconButton>
      </div>
    </div>
  );
}

export default DisplaySelectedImage;
