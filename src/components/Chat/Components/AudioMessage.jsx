import { useRef, useState, useEffect } from "react";
import { IconButton, Slider } from "@material-ui/core";
import {
  PlayCircleFilled,
  PauseCircleFilled,
  Mic,
  Cancel,
} from "@material-ui/icons";
import getBlobDuration from "get-blob-duration";

import Progress from "./Progress";

function sToTime(t) {
  return `${padZero(parseInt((t / 60) % 60))}:${padZero(parseInt(t % 60))}`;
}
function padZero(v) {
  return v < 10 ? "0" + v : v;
}

function AudioMessage({
  audioUrl,
  name,
  createdTime,
  uploadProgress,
  isUpload,
  isReciver,
}) {
  const [audioState, setAudioState] = useState(false);
  const [audioDuration, setAudioDuration] = useState(null);
  const [audioCurrentTime, setAudioCurrentTime] = useState(null);
  const [progressValue, setProgressValue] = useState(0);

  const audioRef = useRef();

  const handleChange = (event, newValue) => {
    const currentTime = ((newValue * audioDuration) / 100).toFixed(6);
    audioRef.current.currentTime = currentTime;
    setProgressValue(newValue);
  };

  useEffect(() => {
    if (audioUrl) {
      audioRef.current.src = audioUrl;
    }
  }, [audioUrl]);

  const handleAudioState = (e) => {
    setAudioState((prevState) => !prevState);
    if (!audioState) {
      audioRef.current.play();
    } else {
      audioRef.current.pause();
    }
  };

  const handleLoadAudio = async () => {
    const duration = await getBlobDuration(audioUrl);
    setAudioDuration(duration);
  };

  const handleChangeCurrenTime = (e) => {
    const currentTime = e.target.currentTime;
    const progress = Math.floor((currentTime / audioDuration) * 100);
    setProgressValue(progress);
    setAudioCurrentTime(currentTime);
  };

  const handleEndedAudio = (e) => {
    setAudioState(false);
    setProgressValue(0);
  };

  const cancelUpload = () => {};

  return (
    <div className={`chat__audio ${isReciver ? "chat__reciver" : ""}`}>
      <span className="chat__name">{name}</span>
      <Mic className="micIcon" />
      {isUpload ? (
        <Progress value={uploadProgress}>
          <IconButton onClick={cancelUpload} className="audio__btn">
            <Cancel />
          </IconButton>
        </Progress>
      ) : (
        <>
          {!audioState && (
            <IconButton onClick={handleAudioState} className="audio__btn">
              <PlayCircleFilled />
            </IconButton>
          )}
          {audioState && (
            <IconButton onClick={handleAudioState} className="audio__btn">
              <PauseCircleFilled />
            </IconButton>
          )}
        </>
      )}

      <div className="audio__info">
        <Slider
          value={progressValue}
          onChange={handleChange}
          aria-labelledby="continuous-slider"
          disabled={isUpload}
        />
        {!audioState && (
          <p className="audio__length">{sToTime(audioDuration)}</p>
        )}
        {audioState && (
          <p className="audio__length">{sToTime(audioCurrentTime)}</p>
        )}
        <p className="audio__sendingTime">{createdTime}</p>
      </div>
      <figure style={{ display: "none" }}>
        <audio
          ref={audioRef}
          onLoadedMetadata={handleLoadAudio}
          onTimeUpdate={handleChangeCurrenTime}
          onEnded={handleEndedAudio}
        ></audio>
      </figure>
    </div>
  );
}

export default AudioMessage;
