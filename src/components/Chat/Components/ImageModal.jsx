import { forwardRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Button,
  Dialog,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Slide,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import Carousel from "react-material-ui-carousel";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "relative",
    backgroundColor: "green",
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  content: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
    overflowY: "auto",
    background: "#f4f4f4",
    flex: 1,
    "& .sliderImage": {
      width: "100%",
      height: "100%",
      padding: "16px",
      display: "flex",
      flex: 1,
      boxSizing: "border-box",
      justifyContent: "center",
      alignItems: "center",
    },
    "& .sliderImage .CarouselItem": {
      height: "100%",
    },
    "& .sliderImage .CarouselItem > div": {
      height: "100%",
      display: "flex",
    },
  },
  image: {
    maxWidth: "100%",
    maxHeight: "100%",
  },
}));

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const protocol = window.location.href.split("/").shift();

function Image({ src }) {
  const classes = useStyles();
  return <img src={src} className={classes.image} alt="imageItem" />;
}

function ImageModal({
  title,
  isOpen,
  onClose,
  images,
  selectedImage,
  onSaveClick,
  onNext,
  onPrev,
  currentImage,
}) {
  const classes = useStyles();

  const selectedImageIndex = selectedImage
    ? images.findIndex((image) => image.id === selectedImage.id)
    : null;

  return (
    <div>
      <Dialog
        fullScreen
        open={isOpen}
        onClose={onClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={onClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              {title}
            </Typography>

            <Button autoFocus color="inherit" onClick={onSaveClick}>
              save
            </Button>
          </Toolbar>
        </AppBar>
        <div className={classes.content}>
          <Carousel
            index={selectedImageIndex ? selectedImageIndex : 0}
            autoPlay={false}
            indicators={false}
            animation="slide"
            className="sliderImage"
            next={onNext}
            prev={onPrev}
          >
            {images.map((image, index) => (
              <Image key={`${image.id}-modalImg-${index}`} src={image.url} />
            ))}
          </Carousel>
        </div>
      </Dialog>
    </div>
  );
}

export default ImageModal;
