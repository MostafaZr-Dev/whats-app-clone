import { useRef } from "react";
import "emoji-mart/css/emoji-mart.css";
import { Picker } from "emoji-mart";
import { ClickAwayListener } from "@material-ui/core";

function EmojiPicker({ isOpen, onOutsideClick, onSelect, ...props }) {
  const pickerRef = useRef();

  return (
    <>
      {isOpen ? (
        <ClickAwayListener onClickAway={onOutsideClick}>
          <div ref={pickerRef} className="emojiPicker">
            <Picker
              {...props}
              set="apple"
              onSelect={onSelect}
              title="Pick your emoji…"
              emoji="point_up"
            />
          </div>
        </ClickAwayListener>
      ) : null}
    </>
  );
}

export default EmojiPicker;
