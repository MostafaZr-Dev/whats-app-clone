import React, { useEffect, useState, useCallback, useRef } from "react";

import Message from "./Message";
import AudioMessage from "./AudioMessage";
import { useAppState } from "../../../store/useAppState";
import { formatDate, formatTime } from "../../../services/date";
import DisplaySelectedImage from "./DisplaySelectedImage";
import ImageMessage from "./ImageMessage";
import ImageModal from "./ImageModal";
import { Image } from "@material-ui/icons";

function ChatBody({
  roomId,
  messages,
  dummyMessage,
  audioUrl,
  isUpload,
  uploadProgress,
  selectedImage,
  onCancelSendImage,
  onSendImage,
  roomName,
}) {
  const {
    state: { user },
    httpService,
  } = useAppState();

  const [isImageModalOpen, setIsImageModalOpen] = useState(false);
  const [selectedMessage, setSelectedMessage] = useState(null);
  const [currentImage, setCurrentImage] = useState(null);

  const images =
    messages?.length >= 1
      ? messages.filter((message) => message.type === "image")
      : [];

  const downloadRef = useRef();

  useEffect(() => {
    if (document.querySelector(".chat__log")) {
      const chatLog = document.querySelector(".chat__log");
      chatLog.scrollTop = chatLog.scrollHeight;
    }
  }, [messages, dummyMessage, selectedImage, audioUrl, isUpload]);

  const handleSendImage = useCallback(
    (e) => {
      onSendImage();
    },
    [selectedImage]
  );

  const onMessageClick = useCallback((messageId, messageUrl) => {
    setSelectedMessage({ id: messageId, url: messageUrl });
    setCurrentImage({ id: messageId, url: messageUrl });
  }, []);

  const handleClickImageModal = useCallback(() => {
    setIsImageModalOpen(true);
  }, []);

  const handleCloseImageModal = useCallback(() => {
    setIsImageModalOpen(false);
  }, []);

  const handleNextImage = useCallback((nextIndex, activeIndex) => {
    const currentImage = images[nextIndex];
    setCurrentImage({
      id: currentImage.id,
      url: currentImage.url,
    });
  }, []);

  const handlePrevImage = useCallback((prevIndex, activeIndex) => {
    const currentImage = images[prevIndex];
    setCurrentImage({
      id: currentImage.id,
      url: currentImage.url,
    });
  }, []);

  const onDownloadImage = useCallback(
    (e) => {
      httpService
        .post(
          `/room/${roomId}/download`,
          {
            imageUrl: currentImage?.url,
          },
          { responseType: "blob" }
        )
        .then((res) => {
          const url = window.URL.createObjectURL(new Blob([res.data]));
          downloadRef.current.href = url;
          const fileExtension = currentImage?.url.split(".").pop();
          const fileName = `whats-app-img-${Math.floor(
            Math.random() * 5000
          )}.${fileExtension}`;
          downloadRef.current.setAttribute("download", fileName);
          downloadRef.current.click();
        });
    },
    [currentImage]
  );

  if (dummyMessage && messages?.length === 0) {
    return (
      <div
        className="chat__body"
        style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/bg.png)` }}
      >
        <div className="chat__log">
          <Message
            name={user.userName}
            message={dummyMessage}
            timestamp={formatDate(new Date())}
            isReciver={false}
            isLoading={!!dummyMessage}
          />
        </div>
      </div>
    );
  }

  if (isUpload && selectedImage.url && messages?.length === 0) {
    return (
      <div
        className="chat__body"
        style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/bg.png)` }}
      >
        <div className="chat__log">
          <ImageMessage
            isReciver={false}
            src={selectedImage.url}
            name={user.userName}
            isUpload={isUpload}
            uploadProgress={uploadProgress}
            timestamp={formatDate(new Date())}
          />
        </div>
      </div>
    );
  }

  if (isUpload && audioUrl && messages?.length === 0) {
    return (
      <div
        className="chat__body"
        style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/bg.png)` }}
      >
        <div className="chat__log">
          <AudioMessage
            name={user.userName}
            audioUrl={audioUrl}
            createdTime={formatTime(new Date())}
            uploadProgress={uploadProgress}
            isUpload={isUpload}
          />
        </div>
      </div>
    );
  }

  if (messages?.length === 0) {
    return (
      <div
        className="chat__body chat__body--empty"
        style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/bg.png)` }}
      >
        {selectedImage.file && (
          <DisplaySelectedImage
            src={selectedImage.url}
            onClose={onCancelSendImage}
            onAccept={handleSendImage}
          />
        )}
        <h5>no messages exist...</h5>
      </div>
    );
  }

  return (
    <div
      className="chat__body"
      style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/bg.png)` }}
    >
      <a ref={downloadRef} style={{ display: "none" }}></a>
      <ImageModal
        isOpen={isImageModalOpen}
        selectedImage={selectedMessage}
        onClose={handleCloseImageModal}
        images={images}
        title={`Image in ${roomName}`}
        onNext={handleNextImage}
        onPrev={handlePrevImage}
        currentImage={currentImage}
        onSaveClick={onDownloadImage}
      />
      {selectedImage.file && (
        <DisplaySelectedImage
          src={selectedImage.url}
          onClose={onCancelSendImage}
          onAccept={handleSendImage}
        />
      )}
      <div className="chat__log">
        {messages?.map((message, index) => (
          <React.Fragment key={message.id}>
            {message.type === "text" && (
              <Message
                key={`${message.id}-txt`}
                name={message.userName}
                message={message.message}
                timestamp={formatDate(message.timestamp)}
                isReciver={message.userId !== user.id}
              />
            )}
            {message.type === "audio" && (
              <AudioMessage
                key={`${message.id}-audio`}
                name={message.userName}
                audioUrl={message.url}
                createdTime={formatTime(message.timestamp)}
                uploadProgress={null}
                isUpload={false}
                isReciver={message.userId !== user.id}
              />
            )}
            {message.type === "image" && (
              <ImageMessage
                key={`${message.id}-img`}
                isReciver={message.userId !== user.id}
                src={message.url}
                name={message.userName}
                isUpload={false}
                uploadProgress={null}
                timestamp={formatTime(message.timestamp)}
                onClick={(e) => {
                  handleClickImageModal();
                  onMessageClick(message.id, message.url);
                }}
              />
            )}
          </React.Fragment>
        ))}
        {dummyMessage && (
          <Message
            name={user.userName}
            message={dummyMessage}
            timestamp={formatDate(new Date())}
            isReciver={false}
            isLoading={!!dummyMessage}
          />
        )}
        {isUpload && audioUrl && (
          <AudioMessage
            name={user.userName}
            audioUrl={audioUrl}
            createdTime={formatTime(new Date())}
            uploadProgress={uploadProgress}
            isUpload={isUpload}
          />
        )}
        {isUpload && selectedImage.url && (
          <ImageMessage
            isReciver={false}
            src={selectedImage.url}
            name={user.userName}
            isUpload={isUpload}
            uploadProgress={uploadProgress}
            timestamp={formatDate(new Date())}
          />
        )}
      </div>
    </div>
  );
}

export default ChatBody;
