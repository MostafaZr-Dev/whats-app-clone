
import Sidebar from "./Sidebar/Sidebar";
import "./Layout.css";

function Layout({ children }) {
  return (
    <div className="app__body">
      <Sidebar />
      {children}
    </div>
  );
}

export default Layout;
