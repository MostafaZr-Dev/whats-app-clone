import { useState } from "react";
import {
  Chat,
  DonutLarge,
  MoreVert,
  PowerSettingsNew,
} from "@material-ui/icons";
import {
  Avatar,
  IconButton,
  Menu,
  MenuItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import { useHistory } from "react-router-dom";

import { useAppState } from "../../../../store/useAppState";

function SidebarHeader() {
  const [menuEl, setMenuEl] = useState(null);

  const {
    state: { user },
    dispatch,
  } = useAppState();

  const history = useHistory();

  const handleClose = () => {
    setMenuEl(false);
  };

  const handleMoreClick = (e) => {
    setMenuEl(e.currentTarget);
  };

  const handleLogout = () => {
    localStorage.clear("token");
    dispatch({
      type: "SET_USER",
      user: null,
      userLoggedIn: false,
    });
    history.replace("/auth/login");
  };

  return (
    <div className="sidebar__header">
      <div className="sidebar__userInfo">
        <Avatar src={user?.avatar} />
        <span>{user?.userName}</span>
      </div>
      <div className="sidebar__headerRight">
        <IconButton>
          <DonutLarge />
        </IconButton>
        <IconButton>
          <Chat />
        </IconButton>
        <IconButton onClick={handleMoreClick}>
          <MoreVert />
        </IconButton>
        <Menu
          anchorEl={menuEl}
          keepMounted
          open={Boolean(menuEl)}
          onClose={handleClose}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
          getContentAnchorEl={null}
        >
          <MenuItem onClick={handleLogout}>
            <ListItemIcon>
              <PowerSettingsNew />
            </ListItemIcon>
            <ListItemText>Logout</ListItemText>
          </MenuItem>
        </Menu>
      </div>
    </div>
  );
}

export default SidebarHeader;
