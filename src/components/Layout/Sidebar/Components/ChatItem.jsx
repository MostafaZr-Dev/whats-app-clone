import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { Avatar, IconButton } from "@material-ui/core";
import { Delete } from "@material-ui/icons";

import Modal from "../../../Modal";

function ChatItem({ id, name, lastMessage, onDeleteRoom }) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [avatar, setAvatar] = useState("");

  useEffect(() => {
    setAvatar(Math.floor(Math.random() * 5000));
  }, []);

  const handleDeleteRoom = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setIsModalOpen(true);
  };

  const handleClose = () => {
    setIsModalOpen(false);
  };

  const onDeleteRoomHandle = () => {
    onDeleteRoom(id);
    setIsModalOpen(false);
  };

  return (
    <div className="chatItem">
      <NavLink to={`/room/${id}`} activeClassName="chatItem--active">
        <Avatar
          src={`https://avatars.dicebear.com/api/avataaars/${avatar}.svg`}
        />
        <div className="chatItem__info">
          <h2>{name}</h2>
          <p>
            {lastMessage?.type === "text"
              ? lastMessage.message
              : lastMessage?.type === "image"
              ? "Image"
              : lastMessage?.type === "audio"
              ? "Audio Message..."
              : null}
          </p>
        </div>
      </NavLink>
      <div className="chatItem__actions" onClick={handleDeleteRoom}>
        <IconButton aria-label="delete">
          <Delete fontSize="small" />
        </IconButton>
      </div>
      <Modal
        isOpen={isModalOpen}
        onClose={handleClose}
        title={`Delete ${name} Room`}
        contentText="Are you sure delete?"
        content={null}
        onCancelAction={handleClose}
        onAcceptAction={onDeleteRoomHandle}
        btnAcceptText="DELETE"
        btnAcceptColor="secondary"
      />
    </div>
  );
}

export default ChatItem;
