import React from "react";
import { Avatar, CircularProgress } from "@material-ui/core";

function DummyChatItem({ name }) {
  return (
    <div className="chatItem chatItem--dummy">
      <Avatar />
      <div className="chatItem__info">
        <h2>{name}</h2>
        <p>...</p>
      </div>
      <CircularProgress
        size={10}
        style={{
          margin: "15px auto",
          color: "#333",
          position: "absolute",
          left: "7px",
          top: "25%",
        }}
      />
    </div>
  );
}

export default DummyChatItem;
