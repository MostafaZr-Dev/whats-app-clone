import React from "react";

function NewChat({onClick}) {
  return (
    <div className="sidebar__newChat" onClick={onClick}>
      <h2>Add New Room</h2>
    </div>
  );
}

export default NewChat;
