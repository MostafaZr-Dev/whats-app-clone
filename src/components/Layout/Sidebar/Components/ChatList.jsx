import { useEffect, useState, useCallback } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useHistory } from "react-router-dom";

import ChatItem from "./ChatItem";
import { useAppState } from "../../../../store/useAppState";
import DummyChatItem from "./DummyChatItem";

function ChatList({ dummyRoom }) {
  const [isLoading, setIsLoading] = useState(true);
  const { state, dispatch, httpService } = useAppState();

  const history = useHistory();

  useEffect(() => {
    httpService
      .get("/room/list")
      .then((response) => {
        const rooms = response.data;
        dispatch({ type: "INIT-ROOMS", rooms });
        setIsLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setIsLoading(false);
      });
  }, []);

  useEffect(() => {
    if (document.querySelector(".sidebar__chatList")) {
      const chatLog = document.querySelector(".sidebar__chatList");
      chatLog.scrollTop = chatLog.scrollHeight;
    }
  }, [dummyRoom]);

  const onDeleteRoom = useCallback((roomId) => {
    httpService
      .delete(`/room/${roomId}/delete`)
      .then((res) => {
        dispatch({ type: "DELETE-SELECTED-ROOM", roomId: res.data.roomId });
        if (roomId && roomId === res.data.roomId) {
          history.push("/");
        }
      })
      .catch((err) => console.log(err));
  }, []);

  if (isLoading) {
    return (
      <div className="sidebar__chatList">
        <CircularProgress
          style={{ margin: "15px auto", color: "rgb(50 156 63)" }}
        />
      </div>
    );
  }

  if (state.rooms.length === 0) {
    return (
      <div className="sidebar__chatList">
        {!dummyRoom && (
          <h5 style={{ margin: "15px auto", color: "#ddd" }}>
            No Room Exist... Please add new room..
          </h5>
        )}
        {dummyRoom && <DummyChatItem name={dummyRoom} />}
      </div>
    );
  }

  return (
    <div className="sidebar__chatList">
      {state.rooms.map((room) => (
        <ChatItem
          key={room.id}
          id={room.id}
          name={room.name}
          lastMessage={
            room.messages?.length >= 1
              ? room.messages[room.messages.length - 1]
              : "last message"
          }
          onDeleteRoom={onDeleteRoom}
        />
      ))}
      {dummyRoom && <DummyChatItem name={dummyRoom} />}
    </div>
  );
}

export default ChatList;
