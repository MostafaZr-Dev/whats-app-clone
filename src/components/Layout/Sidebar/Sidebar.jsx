import { useState, useEffect } from "react";
import { TextField } from "@material-ui/core";
import "./Sidebar.css";

import SidebarHeader from "./Components/SidebarHeader";
import Search from "./Components/Search";
import ChatList from "./Components/ChatList";
import NewChat from "./Components/NewChat";
import Modal from "../../Modal";
import { useAppState } from "../../../store/useAppState";

function Sidebar() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [roomName, setRoomName] = useState("");
  const [dummyRoom, setDummyRoom] = useState(null);

  const { dispatch, httpService, pusherService } = useAppState();

  useEffect(() => {
    pusherService.bind("new-room", (newRoom) => {
      setDummyRoom(null);
      dispatch({ type: "SET-NEW-ROOM", room: newRoom });
    });
  }, []);

  const handleClose = () => {
    setIsModalOpen(false);
  };

  const handleNewClick = () => {
    setIsModalOpen(true);
  };

  const handleRoomName = (e) => {
    const value = e.target.value;
    if (value.length === 0 || value.trim() === "") {
      setRoomName("");
      return;
    }
    setRoomName(value);
  };

  const saveRoom = () => {
    if (roomName.length === 0) {
      return;
    }

    setDummyRoom(roomName);

    httpService
      .post("/room/new", {
        name: roomName,
      })
      .then((response) => {
        setIsModalOpen(false);
        setRoomName("");
      })
      .catch((err) => console.log(err));
  };


  return (
    <div className="sidebar">
      <SidebarHeader />
      <Search />
      <NewChat onClick={handleNewClick} />
      <Modal
        isOpen={isModalOpen}
        onClose={handleClose}
        title="Add New Room"
        contentText={null}
        content={
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="room name..."
            type="text"
            fullWidth
            onChange={handleRoomName}
            value={roomName}
          />
        }
        onCancelAction={handleClose}
        onAcceptAction={saveRoom}
        btnAcceptText="ADD"
        btnAcceptColor="primary"
      />
      <ChatList dummyRoom={dummyRoom} />
    </div>
  );
}

export default Sidebar;
