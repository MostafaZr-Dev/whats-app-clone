import { Backdrop, CircularProgress } from "@material-ui/core";
import "./Loader.css";

function Loader({isOpen}) {
  return (
    <Backdrop className="Loader" open={isOpen}>
      <CircularProgress color="inherit" />
    </Backdrop>
  );
}

export default Loader;
