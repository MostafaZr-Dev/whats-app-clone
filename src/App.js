import HomePage from "./pages/Home";
import PrivateRoute from "./router/PrivateRoute";

function App() {

  return (
    <PrivateRoute path="/">
      <HomePage />
    </PrivateRoute>
  );
}

export default App;
