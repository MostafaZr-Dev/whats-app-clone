import { useEffect, useCallback, useState } from "react";
import "./Auth.css";
import { Switch, Route, useHistory } from "react-router-dom";

import { useAppState } from "../../store/useAppState";
import Register from "./Components/Register";
import Login from "./Components/Login";

function Auth() {
  const [isAuthStart, setIsAuthStart] = useState(false);

  const { httpService, dispatch } = useAppState();

  const history = useHistory();

  useEffect(() => {
    history.replace("/auth/login");
  }, []);

  const onRegister = useCallback((userData) => {
    setIsAuthStart(true);
    httpService
      .post("/auth/register", userData)
      .then((res) => {
        console.log(res);
        setIsAuthStart(false);
        history.replace("/auth/login");
      })
      .catch((err) => console.log(err));
  }, []);

  const onLogin = useCallback((userData) => {
    setIsAuthStart(true);

    httpService
      .post("/auth/login", userData)
      .then((res) => {
        const { token, user } = res.data;
        localStorage.setItem("token", token);
        setIsAuthStart(false);
        dispatch({ type: "SET_USER", user, userLoggedIn: true });
        history.replace("/");
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className="auth">
      <div className="auth__wrapper">
        <Switch>
          <Route path="/auth/login">
            <Login onLogin={onLogin} isLoading={isAuthStart} />
          </Route>
          <Route path="/auth/register">
            <Register onRegister={onRegister} isLoading={isAuthStart} />
          </Route>
        </Switch>
      </div>
    </div>
  );
}

export default Auth;
