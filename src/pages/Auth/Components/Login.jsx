import { useCallback, useRef } from "react";
import { Button, TextField, CircularProgress } from "@material-ui/core";
import { Formik, Form } from "formik";
import { Link } from "react-router-dom";
import * as Yup from "yup";

const LoginSchema = Yup.object().shape({
  userEmail: Yup.string().email("Invalid userEmail").required("Required"),
  userPassword: Yup.string().min(4, "Too Short!").required("Required"),
});

function Login({ onLogin, isLoading }) {
  const formRef = useRef();

  const handleSubmit = useCallback((values) => {
    onLogin(values);
  }, []);

  const handleClick = useCallback(() => {
    if (formRef.current) {
      formRef.current.handleSubmit();
    }
  }, []);

  return (
    <>
      <h1>Login</h1>
      <Formik
        initialValues={{
          userEmail: "",
          userPassword: "",
        }}
        validationSchema={LoginSchema}
        onSubmit={handleSubmit}
        innerRef={formRef}
      >
        {({ errors, touched, handleChange, handleBlur }) => (
          <Form className="auth__form">
            <TextField
              error={errors.userEmail && touched.userEmail}
              name="userEmail"
              type="email"
              label="Email"
              variant="outlined"
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={
                errors.userEmail && touched.userEmail ? errors.userEmail : null
              }
            />
            <TextField
              error={errors.userPassword && touched.userPassword}
              name="userPassword"
              type="password"
              label="Password"
              variant="outlined"
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={
                errors.userPassword && touched.userPassword
                  ? errors.userPassword
                  : null
              }
            />
          </Form>
        )}
      </Formik>
      <Button
        variant="contained"
        className="auth__btn"
        onClick={handleClick}
        startIcon={isLoading ? <CircularProgress size={20} /> : null}
      >
        Login
      </Button>
      <Link to="/auth/register">
        <Button variant="contained" color="primary">
          Register Page
        </Button>
      </Link>
    </>
  );
}

export default Login;
