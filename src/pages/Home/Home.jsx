import { Route ,Switch } from "react-router-dom";
import "./Home.css";

import Chat from "../../components/Chat";
import Layout from "../../components/Layout";
import NotFound from "../../components/NotFound";

function Home() {
  return (
    <div className="app">
      <Layout>
        <Switch>
          <Route path="/room/:roomId" exact>
            <Chat />
          </Route>
          <Route path="*">
            <NotFound />
          </Route>
        </Switch>
      </Layout>
    </div>
  );
}

export default Home;
