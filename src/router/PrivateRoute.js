import { useEffect, useState } from "react";
import { Switch, Route, useHistory } from "react-router-dom";

import { useAppState } from "../store/useAppState";
import Auth from "../pages/Auth";
import Loader from "../components/Loader";

function PrivateRoute({ children, ...rest }) {
  const [isLoading, setIsLoading] = useState(true);

  const { state, dispatch, httpService } = useAppState();
  const history = useHistory();
  const { userLoggedIn } = state;

  useEffect(() => {
    setIsLoading(true);
    const token = localStorage.getItem("token");
    
    if (!token) {
      dispatch({ type: "SET_USER", user: null, userLoggedIn: false });
    }

    httpService
      .get("/auth/check", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        const { user } = res.data;
        dispatch({ type: "SET_USER", user: user, userLoggedIn: true });
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
        dispatch({ type: "SET_USER", user: null, userLoggedIn: false });
        history.replace("/auth/login");
        console.log(err);
      });
  }, []);

  if (isLoading) {
    return <Loader isOpen={true} />;
  }

  if (!userLoggedIn) {
    return (
      <Switch>
        <Route path="/auth">
          <Auth />
        </Route>
      </Switch>
    );
  }

  return (
    <Switch>
      <Route {...rest}>{children}</Route>
    </Switch>
  );
}

export default PrivateRoute;
